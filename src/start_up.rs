use std::sync::Arc;
use crate::encode::broker::Broker;
use crate::service;

pub async fn  start_1078_media(in_port:String, out_port:String){
    let broker = Arc::new(Broker::new());
    let broker_clone = Arc::clone(&broker);
    let handle_in=tokio::spawn(async move{service::service_in::start(in_port,broker).await});
    let handle_out =tokio::spawn(async move{service::service_out::start(out_port,broker_clone).await });
    let _=tokio::join!(handle_in,handle_out);
}