use tokio::io;
use config::config_model::ConfigModel;
mod decode;
mod config;
mod encode;
mod service;
mod start_up;
#[tokio::main]
async fn main() ->io::Result<()>{
    //读取配置
     let (in_port,out_port)=ConfigModel::get_config("config.xml");
     //启动1078流媒体服务器
     start_up::start_1078_media(in_port, out_port).await;

     Ok(())
}

