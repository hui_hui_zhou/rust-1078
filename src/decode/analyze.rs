use bytes::Bytes;
#[derive(Debug,Clone)]
pub struct Analyze {
    //frame_header: [u8; 4],
    // v: u8,
    // p: u8,
    // x: u8,
    // cc: u8,
    // m: u8,
    pub pt: u8,
    pub sn: u16,
    pub sim: String,
    pub ch: u8,
    pub dt: u8,
    fi: u8,
    timestamp: u64,
    last_i_interval: u16,
    last_interval: u16,
    pub data_len: u16,
    pub data: Vec<u8>,
}
impl  Analyze {
    pub fn new() -> Self {
        Analyze {
            // frame_header: [0; 4],
            // v: 0,
            // p: 0,
            // x: 0,
            // cc: 0,
            // m: 0,
            pt: 0,
            sn: 0,
            sim: String::new(),
            ch: 0,
            dt: 0,
            fi: 0,
            timestamp: 0,
            last_i_interval: 0,
            last_interval: 0,
            data_len: 0,
            data: vec![],
        }
    }

    pub fn analyze16(data: Bytes) -> Result<Self, String> {
        let mut header = Self::new();
        // header.frame_header.copy_from_slice(&data[0..4]);
        // header.v = data[4] >> 6 ;
        // header.p = (data[4] >> 5) & 0x1;
        // header.x = (data[4] >> 4)  & 0x1;
        // header.cc = data[4] & 0xF;
        // header.m = data[5] >> 7;
        header.pt = data[5] & 0x7F;
        header.sn = ((data[6] as u16) << 8) | data[7] as u16;
        //sim卡号
        let mut sim=String::new();
        for i in 0..6  {
            sim.push_str(&Self::next_bcd(&data[i+8..i+9]));
        }
        header.sim=sim;
        //通道号
        header.ch = data[14];
        //数据类型
        header.dt = data[15] >> 4;
        //分包处理
        header.fi = data[15] & 0xF;
        let data_len_index=match header.dt {
            3 => {
                header.timestamp=Self::set_time_stamp(&data[16..24]);
                24
            },
            4 => 16,
            _ => {
                header.timestamp=Self::set_time_stamp(&data[16..24]);
                header.last_i_interval = ((data[24] as u16) << 8) | data[25] as u16;
                header.last_interval = ((data[26] as u16) << 8) | data[27] as u16;
                28
            }, // 16,
        };
        header.data_len = ((data[data_len_index] as u16) << 8) | data[data_len_index+1] as u16;
        header.data.extend_from_slice(&data[data_len_index+2..(header.data_len)as usize+data_len_index+2]);
        Ok(header)
    }

    pub fn analyze19(data: Bytes) -> Result<Self, String> {
        // if data.len() < 34 {
        //     return Err("数据长度不足".to_string());
        // }
        let mut header = Self::new();
        // header.frame_header.copy_from_slice(&data[0..4]);
        // header.v = data[4] >> 6;
        // header.p = (data[4] >> 5) & 0x1;
        // header.x = (data[4] >> 4)  & 0x1;
        // header.cc = data[4] & 0xF;
        // header.m = data[5] >> 7;
        header.pt = data[5] & 0x7F;
        header.sn = ((data[6] as u16) << 8) | data[7] as u16;
        //sim卡号
        let mut sim=String::new();
        for i in 0..10  {
            sim.push_str(&Self::next_bcd(&data[i+8..i+9]));
        }
        header.sim=sim;
        //通道号
        header.ch = data[18];
        //数据类型
        header.dt = data[19] >> 4;
        //分包处理
        header.fi = data[19] & 0xF;
        let data_len_index=match header.dt {
            3 => {
                header.timestamp=Self::set_time_stamp(&data[20..28]);
                28
            },
            4 => 20,
            _ => {
                header.timestamp=Self::set_time_stamp(&data[20..28]);
                header.last_i_interval = ((data[28] as u16) << 8) | data[29] as u16;
                header.last_interval = ((data[30] as u16) << 8) | data[31] as u16;
                32
            }, // 16,
        };
        header.data_len = ((data[data_len_index] as u16) << 8) | data[data_len_index+1] as u16;
        header.data.extend_from_slice(&data[data_len_index+2..data_len_index+2 + (header.data_len) as usize]);
        Ok(header)
    }

    fn set_time_stamp( data:&[u8])->u64 {
        let mut result: u64 = 0;
        for &byte in data.iter() {
            result = (result << 8) | u64::from(byte);
        }
        result
    }

    fn next_bcd(data: &[u8]) -> String {
        let val = data[0];
        let ch1 = (val >> 4) & 0x0F;
        let ch2 = val & 0x0F;
        return format!("{}{}", ch1, ch2);
    }

}

