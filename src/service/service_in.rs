use std::net::SocketAddr;
use std::sync::Arc;
use tokio::net::TcpListener;
use crate::decode::decode::Jt1078Codec;
use crate::encode::broker::Broker;

pub async fn start(in_port:String, broker: Arc<Broker>) {
    let addr: SocketAddr = format!("{}:{}", "127.0.0.1", in_port).parse().expect("配置文件地址输入错误");
    let listener = TcpListener::bind(addr).await.expect("设备连接的服务器启动失败");
    println!("1078流媒体输入:{}", in_port);
    while let Ok((socket, _)) = listener.accept().await {
        let broker_clone = Arc::clone(&broker);
        tokio::spawn(async move {
            let mut decode = Jt1078Codec::new();
            decode.handle_connection(socket,broker_clone).await; // 使用 socket 的克隆
        });
    }
}
