use std::net::SocketAddr;
use std::sync::Arc;
use warp::Filter;
use crate::encode::broker::Broker;

pub async fn start(out_port: String, broker_clone: Arc<Broker>){
    let addr: SocketAddr = format!("{}:{}", "127.0.0.1", out_port).parse().unwrap();
    let subscribe = warp::path!("video" / String).and(warp::ws()).map(
        move |topic: String, ws: warp::ws::Ws| {
            let broker_bak = Arc::clone(&broker_clone);
            ws.on_upgrade(move |socket| async move {
                broker_bak.subscribe(topic.clone(), socket).await;
            })
        },
    );
    println!("1078流媒体输出:{}",out_port);
    warp::serve(subscribe).run(addr).await;


}

